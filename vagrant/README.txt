0. put ssh keys to /vagrant/ssh and set up config file
1. vagrant up && vagrant ssh
2. set up ssh keys (see /vagrant/init/ssh). FIXME: script does not work, do it manually
3. git clone and npm install api and client app
4. deploy to heroku (/vagrant/init/deploy_to_heroku)
5. dump tables to postgres db (cd /fs/ramp-api && node create-lb-tables.js) or manually (cd /fs/ramp-api && export HOST=0.0.0.0 && export PORT=3000 && slc arc)
